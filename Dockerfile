FROM nginx:1.23.4

RUN mkdir -p /data/nginx/cache

COPY nginx.conf /etc/nginx/
COPY conf.d/logging-system.conf /etc/nginx/conf.d/

# forward request and error logs to docker log collector
RUN ln -sf /dev/stdout /var/log/nginx/access.log \
	&& ln -sf /dev/stderr /var/log/nginx/error.log

EXPOSE 80

STOPSIGNAL SIGTERM

CMD ["nginx", "-g", "daemon off;"]
